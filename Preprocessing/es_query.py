from elasticsearch import Elasticsearch
import xml.etree.ElementTree as ET
import json
import os

URL = "localhost:8991"
INDEX = "trec_mis_ls"
# INDEX = "trec_mis_short"
TOPICS = "/Data/topics/misinfo-2021-topics.xml"

es = Elasticsearch([URL],
                   retry_on_timeout=True,
                   timeout=1000,
                   max_retries=10)

def load_topics(query_topics: str) -> dict:
    """
    load TREC topics from XML
    :param query_topics: XML file with queries
    :return: a dictionnary with the query text
    """
    tree = ET.parse(query_topics)
    root = tree.getroot()
    topics = {}
    for topic in root.findall("topic"):
        number: str = topic.find("number").text
        query: str = topic.find("query").text
        description: str = topic.find("description").text
        topics[number] = {"query": query, "description": description}
    return topics


def build_query(topics: dict, number: str, include_sites: bool = False) -> str:
    """
    build a match query for a query id
    :param topics: the dictionary obtained from load_topics
    :param number: the topic id
    :return: the json text for the query
    """
    # fields: list = ["description"]
    fields: list = ["query", "description"]
    doc_fields: str = ["text"]
    # doc_fields: str = ["text", "query"]
    # doc_fields: str = ["text", "keywords", "query"]
    topic: dict = topics[number]

    matches: list = []
    query: str = topic[fields[0]] + " " + topic[fields[1]]

    # for doc_field in doc_fields:
    #     matches.append({"match": {doc_field: topic[fields[0]]}})
    #     matches.append({"match": {doc_field: topic[fields[1]]}})
    #
    # if "query" in doc_fields:
    #     matches.append({"match": {"all": query}})
    # else:
    #     matches.append({"match": {"text": query}})

    if include_sites:
        sites = []
        with open("./scrapping/domains_new") as fin:
            for line in fin:
                line = line.strip().replace(".com", "")
                sites.append(line)
        site_query = " ".join(sites)
        query += site_query # matches.append({"match": {"text": site_query}})
        matches.append({"match": {"url": site_query}})

    if "query" in doc_fields:
        matches.append({"match": {"all": query}})
    else:
        matches.append({"match": {"text": query}})

    body: dict = {"query": {"bool": {}}}
    body["query"]["bool"]["should"] = matches

    return json.dumps(body)


def get(id: str) -> str:
    res = es.get(index=INDEX, id=id)
    return res['_source']['text']


def search(query: dict, size: int = 100000, include_source: bool = False, request_timeout: int = 1000, index: str = INDEX) -> list:
    # {"query": {"match_all": {}}}
    res = es.search(index=index, body=query, size=size, request_timeout=request_timeout)
    print("Got %d Hits:" % res['hits']['total']['value'])
    results: list = []
    for hit in res['hits']['hits']:
        doc: list = [hit["_source"]["docno"], hit["_score"]]
        if include_source:
            doc.append(hit["_source"])
        results.append(doc)
    return results


def run(topics: dict, size: int = 10, include_source: bool = False, index: str = INDEX, include_sites: bool = False) -> list:
    results: dict = {}
    count: int = 0
    for tid in topics.keys():
        query = build_query(topics, tid, include_sites=include_sites)
        query_results = search(query, size=size, include_source=include_source, index=index)
        results[tid] = query_results
        count += 1
        if count % 10 == 0:
            print("%d queries processed" % count)
    return results


def write_results(results: dict, output_file: str):
    with open(output_file, mode="w") as fin:
        for tid in sorted(results.keys()):
            rank: int = 1
            for line in results[tid]:
                print("%s Q0 %s %s %s tag" % (tid, line[0], rank, line[1]), file=fin)
                rank += 1


def write_collection(results: dict, output_folder: str):
    for tid in sorted(results.keys()):
        query_folder: str = output_folder + "/" + tid
        if not os.path.isdir(query_folder):
            os.makedirs(query_folder)

        for line in results[tid]:
            doc: str = line[2]
            output_file: str = query_folder + "/" + doc["docno"] + ".json"
            with open(output_file, mode="w") as fin:
                print(json.dumps(doc), file=fin)


if __name__ == "__main__":

    training: bool = False
    include_sites: bool = True
    include_source: bool = True
    size: int = 10000

    output = "./results/trec_bm25"
    output_collection = "./collection"

    if include_sites:
        output += "_sites"
        output_collection += "_sites"

    if training:
        size = 100
        output += "_training"
        output_collection += "_training"

    topics: dict = load_topics(TOPICS)

    results = run(topics, size=size, include_source=include_source, include_sites=include_sites)

    write_results(results, output)

    if include_source:
        write_collection(results, output_collection)