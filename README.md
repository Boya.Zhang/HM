# Online health search via multi-dimensional information quality assessment based on deep language models

This is the code repository for the paper: 
Boya Zhang, Nona Naderi, Rahul Mishra,and Douglas Teodoro.
'Online health search via multi-dimensional information quality assessment based on deep language models.'

## Data Source

Topics, qrels, and annotated documents: [TREC Health Misinformation Track](https://trec-health-misinfo.github.io/)

Web collections: [ClueWeb12-B13](https://lemurproject.org/clueweb12/) and [C4](https://www.tensorflow.org/datasets/catalog/c4) (en.noclean).

## Environment

We use Python 3.11.4 with the following main packages.

## Usage

### Stage 1: Preprocessing

### Stage 2: Transfer Learning

### Stage 3: Domain Adaptation


## Citation

Please cite the following paper.

## Contact

If you have questions regarding this repository, please contact boya.zhang@unige.ch
