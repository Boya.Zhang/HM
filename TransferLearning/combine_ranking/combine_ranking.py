import os
import pandas as pd
from trectools import TrecRun, fusion

class TrecRunProcessor:
    def __init__(self, source_folders, default_size=1000, rrf_k=60, max_docs=10000):
        self.source_folders = source_folders
        self.default_size = default_size
        self.rrf_k = rrf_k
        self.max_docs = max_docs

    def read_run(self, filename):
        names = ["query", "q0", "docid", "rank", "score", "system"]
        return pd.read_csv(filename, sep="\s+", names=names)

    def save_run(self, run_df, filename):
        run_df.to_csv(filename, sep=" ", header=False, index=False)
    
    def clean_run(self, run_file, size=None, tag=None):
        if size is None:
            size = self.default_size
        run_df = self.read_run(run_file)
        run_df = run_df.groupby("query").head(size)
        run_df["query"] = run_df["query"].astype(int)
        if tag is not None:
            run_df["system"] = tag
        return run_df

    def reciprocal_rank_fusion(self, folder):
        runs = []
        for run in os.listdir(folder):
            if os.path.isfile(os.path.join(folder, run)):
                runs.append(TrecRun(os.path.join(folder, run)))

        # Fuse existing runs:
        fused_run = fusion.reciprocal_rank_fusion(runs, k=self.rrf_k, max_docs=self.max_docs)
        self.save_run(fused_run.run_data, os.path.join(folder, f"rrf_{self.rrf_k}"))

        # Clean runs
        for run in os.listdir(folder):
            filename = os.path.join(folder, run)
            df = self.clean_run(filename, size=self.default_size, tag=run)
            self.save_run(df, filename + ".clean")

if __name__ == "__main__":
    # Constants and Configurations
    SOURCE_FOLDERS = [
        "./rankings/rrf_credible",
        "./rankings/rrf_preprocess",
        "./rankings/rrf_support",
        "./rankings/rrf_useful",
        "./rankings/rrf_preprocess_useful/",
        "./rankings/rrf_preprocess_useful_support/",
        "./rankings/rrf_preprocess_useful_support_credible/"
    ]

    # Initialize the processor
    processor = TrecRunProcessor(SOURCE_FOLDERS)

    # Process runs
    for source_folder in SOURCE_FOLDERS:
        processor.reciprocal_rank_fusion(source_folder)

# the 'rrf_60.clean' in each folder are the generated ranking combination
# this code needs improvements to put the combined ranks in a seperate folder
# and set the proper naming directly according to which rankings are being combined