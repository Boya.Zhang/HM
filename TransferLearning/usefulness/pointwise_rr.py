import datetime
import json
import os
import sys
import numpy as np

import pandas as pd
import torch
from transformers import AutoTokenizer, AutoModelForSequenceClassification

from create_runs import save_run
from Preprocessing.es_query import load_topics, TOPICS


def save_results(model_labels, results, output_folder, model_name):
    doc_folder: str = output_folder
    if not os.path.isdir(doc_folder):
        os.makedirs(doc_folder)

    topic_ids, docnos, labels = [], [], []

    with open(results) as fin:
        for line in fin:
            # 101 Q0 en.noclean.c4-train.04871-of-07168.129759 1 217.01797 tag
            topic_id, _, docno, _ = line.strip().split(maxsplit=3)
            if topic_id not in topic_ids:
                labels += model_labels[topic_id]
            topic_ids.append(topic_id)
            docnos.append(docno)

    run_df = pd.DataFrame(
        data={"query": topic_ids, "q0": ["Q0"] * len(topic_ids), "docid": docnos,
              "rank": [1] * len(topic_ids), "score": labels, "tag": [model_name] * len(topic_ids)})

    run_df = run_df.sort_values(["query", "score"], inplace=False, ascending=[True, False])
    run_df["rank"] = run_df.groupby("query")["rank"].cumsum()

    filename: str = os.path.join(output_folder, model_name)
    save_run(run_df, filename)
    print("[%s] run saved to %s" % (datetime.datetime.now(), filename))


def format_input_window(topic, doc, window_size=5, cutoff=5):
    if "all" in doc:
        text = doc["all"]
    else:
        text = doc["text"]

    ## We split the document into sentences
    sentences = []
    for sentence in text.split("\n"):
        if len(sentence.strip()) > 0 and (cutoff <= 0 or len(sentence.split()) > cutoff):
            sentences.append(sentence.strip())

    # We combine up to window_size sentences into a passage. You can choose smaller or larger values for window_size
    # Smaller value: Context from other sentences might get lost
    # Lager values: More context from the paragraph remains, but results are longer
    pairs = []
    for start_idx in range(0, len(sentences), window_size):
        end_idx = min(start_idx + window_size, len(sentences))
        pairs.append([topic, " ".join(sentences[start_idx:end_idx])])

    return pairs


def format_input(model_name, topic, doc):
    if "cross-encoder" in model_name:
        return format_cross_encoder(topic, doc)
    else:
        return format_input_window(topic, doc)
        # return format_bert_sequence(topic, doc)


def format_bert_sequence(topic, doc):
    pairs = [[topic, doc["text"]]]
    if "keywords" in doc:
        pairs.append([topic, doc["keywords"]])
    if "query" in doc:
        pairs.append([topic, doc["query"]])
    return pairs


def format_cross_encoder(topic, doc):
    pairs = [[topic], [doc["text"]]]
    if "keywords" in doc:
        pairs[0].append(topic)
        pairs[1].append(doc["keywords"])
    if "query" in doc:
        pairs[0].append(topic)
        pairs[1].append(doc["query"])
    return pairs


def predict(model, tokenizer, device, topics, docs):
    model_name: str = model.name_or_path
    for i in range(len(docs)):
        try:
            pairs = format_input(model_name, topics[i], docs[i])
            if "cross-encoder" in model_name:
                features = tokenizer(pairs[0], pairs[1], padding=True, truncation=True, return_tensors="pt")
            else:
                features = tokenizer(pairs, padding=True, truncation=True, return_tensors="pt")

            features.to(device)
            with torch.no_grad():
                logits = model(**features).logits
                # Move logits and labels to CPU
                logits = logits.detach().cpu().numpy()
                yield logits
        except Exception as e:
            print("[%s] error processing %s" %(datetime.datetime.now(), i))
            yield np.array([0])


def cross_encoder(model, tokenizer, device, topics, docs):
    predictions_labels = []
    for logits in predict(model, tokenizer, device, topics, docs):
        # get predicitons to list
        # predict_content = logits.argmax(axis=-1).flatten().tolist()
        predict_content = logits.mean().flatten().tolist()
        # update list
        predictions_labels += predict_content
    return predictions_labels


def sequence_classification(model, tokenizer, device, topics, docs):
    predictions_labels = []
    for logits in predict(model, tokenizer, device, topics, docs):
        # get predicitons to list
        predict_content = logits.argmax(axis=-1).flatten().tolist()
        # update list
        # predictions_labels += [1] if sum(predict_content) >= 2 else [0]
        predictions_labels += [sum(predict_content)/len(predict_content)]
    return predictions_labels


def classify(model, tokenizer, device, topics, docs):
    if "cross-encoder" in model_name:
        predictions_labels = cross_encoder(model, tokenizer, device, docs)
    else:
        predictions_labels = sequence_classification(model, tokenizer, device, topics, docs)
    return predictions_labels


def load_results(results, source_folder):
    print("[%s] loading topics" % datetime.datetime.now())
    topics: dict = load_topics(TOPICS)
    for k, v in topics.items():
        topics[k] = v["query"] + " " + v["description"]

    print("[%s] loading documents" % datetime.datetime.now())

    docs: dict = {}
    with open(results) as fin:
        for line in fin:
            # 101 Q0 en.noclean.c4-train.05490-of-07168.44255 6 210.16371 tag
            tid, _, docno, _ = line.split(" ", maxsplit=3)
            if docno not in docs:
                folder: str = docno[20:25]
                doc_file: str = source_folder + "/" + folder + "/" + docno + ".json"
                try:
                    with open(doc_file) as df:
                        if tid in docs:
                            docs[tid].append(json.load(df))
                        else:
                            docs[tid] = [json.load(df)]
                except Exception as e:
                    print("[%s] cannot open doc: %s" % (datetime.datetime.now(), doc_file))
    return topics, docs


def simple_test():
    topic = """ankle brace achilles tendonitis Will wearing an ankle brace help heal achilles tendonitis?"""

    all = """Best Ankle Brace for Achilles Tendonitis - Help My Foot Pain\nSkip to content\nHome\nFoot Health\nReviews\nAbout\nDisclamer\nHome\nFoot Health\nReviews\nAbout\nDisclamer\nBest Ankle Brace for Achilles Tendonitis\nApril 9, 2019 June 4, 2018 by Kyle\nAchilles Tendonitis\nThe Achilles tendon is the largest tendon in the human body. It connects your heel bone to your calf muscles and is used when walking, running, and jumping. Achilles tendonitis occurs when the Achilles tendon becomes inflamed or aggravated. Achilles tendonitis occurs in the middle area of the tendon which is slightly above the heel, and typically affects athletes and younger individuals.\nAchilles tendonitis can also occur in anyone, even people who are not active and occurs on the lower portion of the heel. For more information, check out this post on Achilles tendonitis.\nBest Ankle Brace for Achilles Tendonitis\nSo you have achilles tendonitis, now what? You more than likely should take a look at getting a brace. A brace will provide compression and support to help with your Achilles tendonitis. So what is the best ankle brace for Achilles tendonitis? We reviewed several braces and we believe the Bauerfeind AchilloTrain, was the best brace we tested. I felt this brace provided the best of both worlds by providing support as well as providing a messaging effect to stop swelling and bruising.\nKey Features of the Best Ankle Brace for Achilles Tendonitis\nCompression\nA good Achilles tendon brace will have good compression. Compression is key to the recovery process and to help against further injury. Compression will guide the stabilization of the joints. This will help with the reduction of swelling and bruises. Also allowing good blood circulation to the area which will help in the healing process.\nBreathability\nBreathability is key in any brace. Especially when it comes to braces being worn around the foot/ankle area. Often these braces are worn under a sock and with shoes on. It is key to have a fabric that is breathable that wicks away moisture, keeping the foot dry and comfortable.\nHeel Support\nA key factor in rehabbing you Achilles is to keep the tension off of the Achilles tendon. A good brace will include some type of heel assist in the brace, either an insertable heel wedge or some type of wedge that is built into the brace itself.\nDurability\nBeing that the brace will be on your foot/ankle area, the brace needs to be durable and hold up to continued use. The brace needs to be made of quality fabrics that will not prematurely wear. The best braces should be durable enough to wear daily while the healing process continues which could last up to six weeks.\nImage\nTitle\nPrice\nPrime\nBuy\nBauerfeind AchilloTrain Buy Now\nCorflex Achimed Achilles Support X Buy Now\nLevamed Ankle Support X Buy Now\nSenteq Ankle Brace Buy Now\nBauerfeind Sports Ankle Support Dynamic Buy Now\nPrices accurate as of:\nBest Ankle Brace for Achilles Tendon Injury In Depth\nName: Bauerfeind AchilloTrain\nSpecs:\nThe brace comes in three different colors. and 6 sizes. It is made from a breathable train active knit. Two viscoelastic pads are integrated into the fabric. It is highly elastic for easy on/off wear.\nFeatures:\nAnatomical optimal shape\nMade of Breathable Materials\nHelps promote circulation via compression\nName: Corflex Achimed Achilles Support\nSpecs:\nThe brace comes in 3 different colors, and 7 Sizes. The brace is made out of polyimide, polyester, and elastane. The fabric has two silicon inlays that are integrated into the fabric. The brace is designed to be worn in comfort all day.\nFeatures:\nMassage effect due to integrated silicone pads -> reduction of swellings (edema, hematomas)\nCompression \u2192 Guidance and stabilization of the joints\nThe optional, insertable heel wedges reduce painful tension on the Achilles tendon\nName: Levamed Ankle Support\nSpecs:\nThe brace comes in 6 sizes and 5 colors. The brace is made out of compression fabric with silicone inserts. The brace also has silicone inserts.\nFeatures:\nAnatomically tailored support with silicone pads\nVery elastic, breathable and moisture-transporting fabric\nSpecially knitted comfort-zone over the instep prevents constriction and pressure areas\nName: Senteq Ankle Brace\nSpecs:\nThe brace comes in two sizes medium and large. The brace comes in only one color. The brace features a lacing and velcro closure. It is designed to be worn with shoes.\nFeatures:\nStabilize weak ankles to prevent sprains, strains and other injuries while maximizing comfort\nFor the treatment of pain related to sprains, strains, arthritis and excessive muscle and joint movement\nAdjustable cross tension straps for added stabilization and support, Breathable material, Fits left or right.\nName: Bauerfeind Sports Ankle Support Dynamic\nSpecs:\nThe brace comes in 6 sizes and two colors. It is made of highly elastic fabric for regulated compression. It is durable and easy to clean.\nFeatures:\nSlim design\nThe specially knit fabric is breathable and durable\nDesigned to promote mobility not restrict it\nWhat People Think\nName: Bauerfeind AchilloTrain\nPeople tend to love the quality of this brace, and think that is a good value.\nMost people who have bought this brace think that you make sure you get the correct measurement of your foot so the brace fits correctly.\nSome people thought that the brace didn\u2019t fit well within some shoes, making them feel that the foot was being forced inward.\nName: Corflex Achimed Achilles Support\nPeople seem to love the support the brace provides.\nMost people who have bought this brace think that you make sure you get the correct measurement of your foot so the brace fits correctly.\nMost believe as I did that the product is superior in constructions and quality.\nName: Levamed Ankle Support\nPeople liked that it was easy to get on and off.\nMost thought that is was easy to wear and was supportive while conducting athletic activities.\nMost thought it made a big difference in the level of pain experienced when walking.\nName: Senteq Ankle Brace\nMost thought it was very affordable when compared to other braces.\nPeople thought it provided good support and foot ankle alignment.\nPeople thought it was easy to put on and take off, though some thought it took a little while to get used the lacing/velcro system.\nName: Bauerfeind Sports Ankle Support Dynamic\nPeople thought the brace was excellent quality for the price.\nMost thought the construction and fit were nice.\nPeople felt the fabric worked well and was very breathable.\nSome thought it wasn\u2019t as supportive if you have had a serious injury.\nWhich One Should You Get?\nSo which brace should you get? Each of the five braces we reviewed was pretty good at supporting the Achilles. Helping make you feel more stable in your day-to-day activities. If you are searching for the Best Ankle Brace for Achilles Tendonitis you want a brace that is:\nSupportive\nProvides Compression\nIs easy to get on and off\nIt is made of a breathable material\nWell made and durable\nIn this case, you couldn\u2019t go wrong with any of these picks, but in our opinion, the Best Ankle Brace for Achilles Tendonitis was the Bauerfeind AchilloTrain . We just felt it met each of the criteria that are important for an Achilles tendon brace to have. If you are having achilles tendon pain, it is important to see a doctor for a checkup. You deserve to feel better and get back to your day-to-day activities. With the right care and treatment plan, you will be back on healthy feet in no time.\nBauerfeind AchilloTrain\nNew knitting concept: improved stretch, breathability and moisture dissipation for effective compression and excellent wearing comfort\nAnatomically contoured active knitted support\nIntegrated anatomically contoured viscoelastic pad\nA removable heel wedge integrated into the support\nA separate heel cushion provided for the unaffected leg to offset the length difference\nView on Amazon\nPrices/Images/Reviews pulled from the Amazon Product Advertising API on:\nProduct prices and availability are accurate as of the date/time indicated and are subject to change. Any price and availability information displayed on the respective Amazon site that you are redirected to at the time of purchase will apply to the purchase of this product.\nShare this:\nClick to share on Twitter (Opens in new window)\nClick to share on Facebook (Opens in new window)\nClick to share on Pinterest (Opens in new window)\nRelated\nCategories ReviewsTags achilles, achilles brace, achilles tendonitis, best ankle brace for achilles tendonitis\tPost navigation\nBest Brace for Achilles Tendon Injury\nKT Tape for Plantar Fasciitis Review\nLeave a Comment Cancel reply\nComment\nName Email Website\nNotify me of follow-up comments by email.\nNotify me of new posts by email.\nProduct Highlight\nBest Walking Boot for Plantar Fasciitis\nLearn more\nRecent Posts\nRigid Ankle Braces for Maximum Support\nBest Rigid Ankle Brace\nAlternative Crutches Help Ease the Pain\nAlternative to Crutches for Foot Injury\nMassaging Your Way Out of Foot Pain\nFollow Me\nFacebook\nPinterest\nDisclaimer\nHelpMyFootPain is a participant in the Amazon Services LLC Associates Program, and as an Amazon Associate, I earn from qualifying purchases. Amazon Associates is an affiliate advertising program designed to provide a means for us to earn fees by linking to Amazon.com and affiliated sites\n\u00a9 2019 Help My Foot Pain \u2022 Powered by GeneratePress\nHome\nReviews\nFoot Health\nAbout\nDisclamer\nClose achilles best tendonitis jumping brace ankle is achilles tendon brace breathable what is the best ankle brace best ankle brace for achilles tendonitis what material are braces made of what kind of brace to use for achilles tendonitis why are ankle braces used for achilles tendonitis? what is the best ankle brace for achilles tendonitis? what is the best ankle brace for achilles tendonitis what kind of brace does bauerfeind use what brace is best for achilles tendonitis"""
    text = """Best Ankle Brace for Achilles Tendonitis - Help My Foot Pain\nSkip to content\nHome\nFoot Health\nReviews\nAbout\nDisclamer\nHome\nFoot Health\nReviews\nAbout\nDisclamer\nBest Ankle Brace for Achilles Tendonitis\nApril 9, 2019 June 4, 2018 by Kyle\nAchilles Tendonitis\nThe Achilles tendon is the largest tendon in the human body. It connects your heel bone to your calf muscles and is used when walking, running, and jumping. Achilles tendonitis occurs when the Achilles tendon becomes inflamed or aggravated. Achilles tendonitis occurs in the middle area of the tendon which is slightly above the heel, and typically affects athletes and younger individuals.\nAchilles tendonitis can also occur in anyone, even people who are not active and occurs on the lower portion of the heel. For more information, check out this post on Achilles tendonitis.\nBest Ankle Brace for Achilles Tendonitis\nSo you have achilles tendonitis, now what? You more than likely should take a look at getting a brace. A brace will provide compression and support to help with your Achilles tendonitis. So what is the best ankle brace for Achilles tendonitis? We reviewed several braces and we believe the Bauerfeind AchilloTrain, was the best brace we tested. I felt this brace provided the best of both worlds by providing support as well as providing a messaging effect to stop swelling and bruising.\nKey Features of the Best Ankle Brace for Achilles Tendonitis\nCompression\nA good Achilles tendon brace will have good compression. Compression is key to the recovery process and to help against further injury. Compression will guide the stabilization of the joints. This will help with the reduction of swelling and bruises. Also allowing good blood circulation to the area which will help in the healing process.\nBreathability\nBreathability is key in any brace. Especially when it comes to braces being worn around the foot/ankle area. Often these braces are worn under a sock and with shoes on. It is key to have a fabric that is breathable that wicks away moisture, keeping the foot dry and comfortable.\nHeel Support\nA key factor in rehabbing you Achilles is to keep the tension off of the Achilles tendon. A good brace will include some type of heel assist in the brace, either an insertable heel wedge or some type of wedge that is built into the brace itself.\nDurability\nBeing that the brace will be on your foot/ankle area, the brace needs to be durable and hold up to continued use. The brace needs to be made of quality fabrics that will not prematurely wear. The best braces should be durable enough to wear daily while the healing process continues which could last up to six weeks.\nImage\nTitle\nPrice\nPrime\nBuy\nBauerfeind AchilloTrain Buy Now\nCorflex Achimed Achilles Support X Buy Now\nLevamed Ankle Support X Buy Now\nSenteq Ankle Brace Buy Now\nBauerfeind Sports Ankle Support Dynamic Buy Now\nPrices accurate as of:\nBest Ankle Brace for Achilles Tendon Injury In Depth\nName: Bauerfeind AchilloTrain\nSpecs:\nThe brace comes in three different colors. and 6 sizes. It is made from a breathable train active knit. Two viscoelastic pads are integrated into the fabric. It is highly elastic for easy on/off wear.\nFeatures:\nAnatomical optimal shape\nMade of Breathable Materials\nHelps promote circulation via compression\nName: Corflex Achimed Achilles Support\nSpecs:\nThe brace comes in 3 different colors, and 7 Sizes. The brace is made out of polyimide, polyester, and elastane. The fabric has two silicon inlays that are integrated into the fabric. The brace is designed to be worn in comfort all day.\nFeatures:\nMassage effect due to integrated silicone pads -> reduction of swellings (edema, hematomas)\nCompression \u2192 Guidance and stabilization of the joints\nThe optional, insertable heel wedges reduce painful tension on the Achilles tendon\nName: Levamed Ankle Support\nSpecs:\nThe brace comes in 6 sizes and 5 colors. The brace is made out of compression fabric with silicone inserts. The brace also has silicone inserts.\nFeatures:\nAnatomically tailored support with silicone pads\nVery elastic, breathable and moisture-transporting fabric\nSpecially knitted comfort-zone over the instep prevents constriction and pressure areas\nName: Senteq Ankle Brace\nSpecs:\nThe brace comes in two sizes medium and large. The brace comes in only one color. The brace features a lacing and velcro closure. It is designed to be worn with shoes.\nFeatures:\nStabilize weak ankles to prevent sprains, strains and other injuries while maximizing comfort\nFor the treatment of pain related to sprains, strains, arthritis and excessive muscle and joint movement\nAdjustable cross tension straps for added stabilization and support, Breathable material, Fits left or right.\nName: Bauerfeind Sports Ankle Support Dynamic\nSpecs:\nThe brace comes in 6 sizes and two colors. It is made of highly elastic fabric for regulated compression. It is durable and easy to clean.\nFeatures:\nSlim design\nThe specially knit fabric is breathable and durable\nDesigned to promote mobility not restrict it\nWhat People Think\nName: Bauerfeind AchilloTrain\nPeople tend to love the quality of this brace, and think that is a good value.\nMost people who have bought this brace think that you make sure you get the correct measurement of your foot so the brace fits correctly.\nSome people thought that the brace didn\u2019t fit well within some shoes, making them feel that the foot was being forced inward.\nName: Corflex Achimed Achilles Support\nPeople seem to love the support the brace provides.\nMost people who have bought this brace think that you make sure you get the correct measurement of your foot so the brace fits correctly.\nMost believe as I did that the product is superior in constructions and quality.\nName: Levamed Ankle Support\nPeople liked that it was easy to get on and off.\nMost thought that is was easy to wear and was supportive while conducting athletic activities.\nMost thought it made a big difference in the level of pain experienced when walking.\nName: Senteq Ankle Brace\nMost thought it was very affordable when compared to other braces.\nPeople thought it provided good support and foot ankle alignment.\nPeople thought it was easy to put on and take off, though some thought it took a little while to get used the lacing/velcro system.\nName: Bauerfeind Sports Ankle Support Dynamic\nPeople thought the brace was excellent quality for the price.\nMost thought the construction and fit were nice.\nPeople felt the fabric worked well and was very breathable.\nSome thought it wasn\u2019t as supportive if you have had a serious injury.\nWhich One Should You Get?\nSo which brace should you get? Each of the five braces we reviewed was pretty good at supporting the Achilles. Helping make you feel more stable in your day-to-day activities. If you are searching for the Best Ankle Brace for Achilles Tendonitis you want a brace that is:\nSupportive\nProvides Compression\nIs easy to get on and off\nIt is made of a breathable material\nWell made and durable\nIn this case, you couldn\u2019t go wrong with any of these picks, but in our opinion, the Best Ankle Brace for Achilles Tendonitis was the Bauerfeind AchilloTrain . We just felt it met each of the criteria that are important for an Achilles tendon brace to have. If you are having achilles tendon pain, it is important to see a doctor for a checkup. You deserve to feel better and get back to your day-to-day activities. With the right care and treatment plan, you will be back on healthy feet in no time.\nBauerfeind AchilloTrain\nNew knitting concept: improved stretch, breathability and moisture dissipation for effective compression and excellent wearing comfort\nAnatomically contoured active knitted support\nIntegrated anatomically contoured viscoelastic pad\nA removable heel wedge integrated into the support\nA separate heel cushion provided for the unaffected leg to offset the length difference\nView on Amazon\nPrices/Images/Reviews pulled from the Amazon Product Advertising API on:\nProduct prices and availability are accurate as of the date/time indicated and are subject to change. Any price and availability information displayed on the respective Amazon site that you are redirected to at the time of purchase will apply to the purchase of this product.\nShare this:\nClick to share on Twitter (Opens in new window)\nClick to share on Facebook (Opens in new window)\nClick to share on Pinterest (Opens in new window)\nRelated\nCategories ReviewsTags achilles, achilles brace, achilles tendonitis, best ankle brace for achilles tendonitis\tPost navigation\nBest Brace for Achilles Tendon Injury\nKT Tape for Plantar Fasciitis Review\nLeave a Comment Cancel reply\nComment\nName Email Website\nNotify me of follow-up comments by email.\nNotify me of new posts by email.\nProduct Highlight\nBest Walking Boot for Plantar Fasciitis\nLearn more\nRecent Posts\nRigid Ankle Braces for Maximum Support\nBest Rigid Ankle Brace\nAlternative Crutches Help Ease the Pain\nAlternative to Crutches for Foot Injury\nMassaging Your Way Out of Foot Pain\nFollow Me\nFacebook\nPinterest\nDisclaimer\nHelpMyFootPain is a participant in the Amazon Services LLC Associates Program, and as an Amazon Associate, I earn from qualifying purchases. Amazon Associates is an affiliate advertising program designed to provide a means for us to earn fees by linking to Amazon.com and affiliated sites\n\u00a9 2019 Help My Foot Pain \u2022 Powered by GeneratePress\nHome\nReviews\nFoot Health\nAbout\nDisclamer\nClose"""
    keywords = "achilles best tendonitis jumping brace ankle"
    query = "is achilles tendon brace breathable what is the best ankle brace best ankle brace for achilles tendonitis what material are braces made of what kind of brace to use for achilles tendonitis why are ankle braces used for achilles tendonitis? what is the best ankle brace for achilles tendonitis? what is the best ankle brace for achilles tendonitis what kind of brace does bauerfeind use what brace is best for achilles tendonitis"

    doc1: dict = {"all": all, "text": text, "keywords": keywords, "query": query}

    all = """Neck Braces and Head Supports - Free Shipping and Easy Returns\nJavaScript seems to be disabled in your browser.\nYou must have JavaScript enabled in your browser to utilize the functionality of this website.\nFREE GROUND SHIPPING\nSupport\nOrder Status\nShipping\nReturns\nMoneyback Guarantee\nFAQ\nContact\nAbout Us\nTalk to an Expert: 800.553.6019\nShop\nBrace Advisor\nSale\nEducation\nSearch\nSearch:\nSearch\nAccount\nMy Account\nRegister\nLog In\nLog Out\nCart\nCheckout\nView Shopping Cart\nShop By\nBody Part\nInjury\nSport\nBrand\nTherapy\nShop By Body Part\nKnee\nShop All Knee\nBraces & Support\nKnee Sleeves\nHinged Braces\nPatella Stabilizers\nBands & Straps\nCustom Knee Braces\nKnee Immobilizers\nKnee Pads\nAccessories\nHot & Cold Therapy\nGet the right brace! Use our Brace Advisor\nAnkle\nShop All Ankle\nWalking Braces\nBraces & Support\nHot & Cold Therapy\nGet the right brace! Use our Brace Advisor\nBack\nShop All Back\nLower Back Braces\nUpper Back / Posture Support\nAbdominal / Rib Support\nHot & Cold Therapy\nGet the right brace! Use our Brace Advisor\nWrist & Thumb\nShop All Wrist & Thumb\nImmobilizers\nBraces & Support\nHot & Cold Therapy\nGet the right brace! Use our Brace Advisor\nShoulder\nShop All Shoulder\nBraces & Support\nImmobilizers / Post-Op\nClavicle Support\nHot & Cold Therapy\nGet the right brace! Use our Brace Advisor\nElbow\nShop All Elbow\nBraces & Support\nProtective Padding\nHot & Cold Therapy\nGet the right brace! Use our Brace Advisor\nNeck\nShop All Neck\nBraces & Support\nHot & Cold Therapy\nGet the right brace! Use our Brace Advisor\nFoot\nShop All Foot\nWalking Boots\nBraces & Support\nShoes\nSocks\nAccessories\nHot & Cold Therapy\nGet the right brace! Use our Brace Advisor\nLeg\nShop All Leg\nBraces & Support\nPost-Op\nHot & Cold Therapy\nGet the right brace! Use our Brace Advisor\nShop All Body Parts >\nShop By Injury\nKnee Injuries\nACL Injury\nChondromalacia\nDislocated Kneecap\nIliotibial Band Syndrome\nMCL/LCL Injury\nMeniscus Tear\nOsteoarthritis of the Knee\nPatellofemoral Pain (Runner's Knee)\nPCL Injury\nPatellar Tendonitis / Jumpers Knee\nHyperextended Knee\nKnee Sprain\nOsgood-Schlatters\nBursitis\nShop All Knee Injuries\nGet the right brace! Use our Brace Advisor\nAnkle Injuries\nAchilles Tendonitis\nAnkle Sprain & Instability\nOsteoarthritis of the Ankle\nShop All Ankle Injuries\nGet the right brace! Use our Brace Advisor\nShoulder Injuries\nRotator Cuff Tear\nShoulder Arthritis\nShoulder Dislocation\nShoulder Instability\nShoulder Impingement\nShoulder/AC Joint Separation\nShop All Shoulder Injuries\nGet the right brace! Use our Brace Advisor\nLeg Injuries\nHamstring Injury\nCalf Strain\nShin Splints\nQuad/Groin Injury\nShop All Leg Injuries\nGet the right brace! Use our Brace Advisor\nBack Injuries\nAbdominal Strains\nHerniated Disc\nLower Back Pain\nSacroiliac Joint Discomfort\nSciatica\nUpper Back Problems\nShop All Back Injuries\nGet the right brace! Use our Brace Advisor\nElbow Injuries\nElbow Arthritis\nElbow Hyperextension\nElbow Tendonitis\nGolfer's Elbow\nTennis Elbow\nShop All Elbow Injuries\nGet the right brace! Use our Brace Advisor\nWrist & Thumb Injuries\nCarpal Tunnel Syndrome\nThumb Injury\nWrist Injury\nShop All Wrist & Thumb Injuries\nGet the right brace! Use our Brace Advisor\nFoot Injuries\nArch Pain\nBall of Foot Pain\nHeel Pain & Spurs\nPlantar Fasciitis\nHigh-Arched Foot / Cavus Foot\nFlat Foot / PTTD\nFoot Arthritis\nDiabetic Foot\nStress Fractures\nBunions\nDrop Foot\nShop All Foot Injuries\nGet the right brace! Use our Brace Advisor\nShop All Injuries >\nShop By Sport\nRunning\nKnee Braces\nAnkle Braces\nCalf & Shin Supports\nFoot Braces\nShop All Running Products\nGet the right brace! Use our Brace Advisor\nBasketball\nKnee Braces\nAnkle Braces\nWrist Braces\nElbow Braces\nCalf & Shin Supports\nShoulder Supports\nShop All Basketball Products\nGet the right brace! Use our Brace Advisor\nLacrosse\nKnee Braces\nAnkle Braces\nElbow Braces\nShoulder Braces\nShop All Lacrosse Products\nGet the right brace! Use our Brace Advisor\nFootball\nKnee Braces\nAnkle Braces\nElbow Supports\nShoulder Braces\nWrist Supports\nBack Braces\nShop All Football Products\nGet the right brace! Use our Brace Advisor\nSoccer\nKnee Braces\nAnkle Braces\nBack Supports\nHead Protection\nShoulder Braces\nWrist Supports\nShop All Soccer Products\nGet the right brace! Use our Brace Advisor\nTennis\nKnee Braces\nAnkle Braces\nElbow Supports\nShop All Tennis Products\nGet the right brace! Use our Brace Advisor\nVolleyball\nKnee Braces\nAnkle Braces\nCalf/Shin Supports\nElbow Braces\nShop All Volleyball Products\nGet the right brace! Use our Brace Advisor\nBaseball\nKnee Braces\nAnkle Braces\nBack Braces\nShoulder Support\nElbow Braces\nWrist Braces\nShop All Baseball & Softball\nGet the right brace! Use our Brace Advisor\nSnowboarding\nKnee Braces\nAnkle Braces\nWrist Supports\nBack Braces\nShop All Snowboarding Products\nGet the right brace! Use our Brace Advisor\nSkiing\nKnee Braces\nAnkle Braces\nBack Braces\nWrist Braces\nShop All Skiing Products\nGet the right brace! Use our Brace Advisor\nHockey\nKnee Braces\nAnkle Braces\nBack Braces\nElbow Braces\nShoulder Braces\nShop All Hockey Products\nGet the right brace! Use our Brace Advisor\nShop All Sports >\nShop By Brand\nDonJoy\nAircast\nDonJoy Advantage\nColpac\nDonJoy Performance\nProcare\nShop All Brands >\nCompex\nHyperIce\nDr Comfort\nChattanooga\nAircast\nDonJoy\nDonJoy Performance\nDonJoy Advantage\nColPac\nProcare\nCompex\nDr Comfort\nExos\nChattanooga\nEmpi\nHyperice\nView All Brands\nShop By Therapy\nTherapy\nHot & Cold Therapy\nColpac Ice Packs\nFast Freeze Pain Relief\nTENS Units\nMuscle Stimulators\nElectrodes\nRehab Products\nProtective Padding\nShop All Therapy >\nHot & Cold Therapy\nColPac Ice Packs\nFast Freeze Pain Relief\nTENS Units\nMuscle Stimulators\nElectrodes\nRehab Products\nProtective Padding\nFitness Roller\nTopical Pain Relief\nShop All\nBrace Advisor\nSale\nEducation\nAbout Us\nCustomer Service\n25% Off Your Purchase + Free Shipping | Code: BBEASTER | SHOP NOW\nOffer Ends 04/22/19 | DETAILS\n25% Off Sitewide* | SHOP NOW\nTake 25% Off Your Order with Code: BBEASTER. Offer ends 04/22/19. Limit 1 coupon per offer. Exclusions apply. Cannot be combined with other coupons or discounts. Not applicable to previous orders. Free ground shipping on all orders in the 48 contiguous states. In-stock, non-custom-non made-to-order products ship in 3-5 business days.\n*Offer excludes Hyperice, DonJoy Performance Bionic\u2122 Reel-Adjust Wrist, DonJoy Performance Bionic\u2122 Reel-Adjust Back, Aircast Walking Boots (Standard, Elite, Short, SP, FP, XP), Aircast Foam Liners, and Compex Edge 2.0. For a complete exclusion list, contact customer service.\nIf you would like to purchase both products eligible for coupon code and products excluded from the promotion, please make two separate transactions. If you have questions, please contact customer service for assistance.\nHome /\nHead & Neck Braces\nHead and Neck Braces & Supports\nCervical collars or neck braces help support and immobilize the neck after injury, neck strain, or whiplash. Whether you need a cervical collar or headgear to protect your head from concussions during soccer, we have the right head or neck support for you. Our bracing experts recommend the following supports for head and neck pain.\nRead More Read Less\nShop By\nCurrently Shopping by\nProduct Type:\nBraces & Supports Remove This Item\nClear All\nFilter\nCategory\nCervical Collars (1)\nProduct Type\nBraces & Supports (1)\nPost-Op (1)\nTherapy Products (3)\nRehab (1)\nCold Therapy (2)\nIntended Use\nDaily Use (1)\nPost-Op/Rehabilitation (1)\nInjuries\nNeck Strain (1)\nBrand\nProCare (1)\nPart of Head & Neck\nNeck (1)\nMaterial\nLatex Free (1)\nBrace Type\nWraparound (1)\nName Price New Best Sellers Top Rated Reviews Count Set Ascending Direction\n1 Item(s)\nShow 12 24 48 96 All\nProCare\nForm Fit Cervical Collar\n$26.99\nExtra 25% Off\nAdd to Cart\nCervical collar to restrict cervical spine flexion, extension and rotation Read More\nSort By Position Name Price New Best Sellers Top Rated Reviews Count Set Ascending Direction\n1 Item(s)\nShow 12 24 48 96 All\nHead and Neck Supports\nNeck braces and cervical collars are ideal for helping heal head and neck injuries by immobilizing the area after an injury has occurred. This is important in order to help prevent movement that could further injure the area. Neck braces are commonly used after events such as car accidents or extreme sports injuries. Paramedics and other types of first responders often use cervical collars to stabilize the neck after an injury. Once the doctor has examined the injury, he or she may choose to keep the patient in the neck brace. They help during rehabilitation by supporting the weight of the head and keeping pressure off any strained neck muscles. A good neck brace or cervical collar not only protects, but they will be comfortable as well. Check out our wide selection of head and neck braces and cervical collars today!\nNewsletter\nSTAY CONNECTED Sign up for the Better Braces Newsletter\nSIGN UP\nBlog\nPinterest\nIntragram\nFacebook\nTwiiter\nYouTube\nBraces & Support\nAnkle\nKnee\nBack Braces\nHead & Neck\nWrist & Thumb\nShoulder\nFoot\nElbow\nLeg Supports\nWalking Boots\nParts & Accessories\nDonJoy Replacement Parts\nAircast Replacement Parts\nCompex Replacement Parts\nTherapy & More\nHot & Cold Therapy\nMuscle Stimulators\nElectrodes\nApparel\nMotionCare\nBrands\nAircast\nDonJoy\nDonJoy Performance\nDonJoy Advantage\nProcare\nDr Comfort\nExos\nCompex\nChattanooga\nEmpi\nHyperice\nFast Freeze\nSaunders\nLearn More\nBlog\nJoin the Club\nShop By Injury\nShop By Sport\nSitemap\nPrivacy Policy\nCustomer Service\nMy Account\nView Cart\nAbout Us\nCustomer Care\nShipping & Rates\nReturns\nContact Us\nCopyright \u00a9 2019 BetterBraces.com\nIndividual results may vary.\nNeither DJO Global, Inc. nor any of its subsidiaries dispense medical advice. The contents of this website do not constitute medical, legal, or any other type of professional advice. Information related to various health, medical, and fitness conditions and their treatment is not meant to be a substitute for the advice provided by a physician or other medical professional. You should not use the information contained herein for diagnosing a health or fitness problem or disease. Rather, please consult your healthcare professional for information on the courses of treatment, if any, which may be appropriate for you.\npowered by Olark live chat software\nPlease wait...\nContinue Shopping Go to Cart\nContinue Shopping Go to Cart\nCall 800-553-6019\nNeed Help?\n\u00d7 brands javascript enabled disabled intragram shop facebook browser therapy twiiter can you use a neck brace for a foot injury when do neck braces stop working what neck braces for athletes for pain how to use cervical collar why use neck braces what kind of braces for knee do cervical braces work? which braces are used to protect the cervical spine which neck braces help does a cervical neck brace help a muscle strain"""
    text = """Neck Braces and Head Supports - Free Shipping and Easy Returns\nJavaScript seems to be disabled in your browser.\nYou must have JavaScript enabled in your browser to utilize the functionality of this website.\nFREE GROUND SHIPPING\nSupport\nOrder Status\nShipping\nReturns\nMoneyback Guarantee\nFAQ\nContact\nAbout Us\nTalk to an Expert: 800.553.6019\nShop\nBrace Advisor\nSale\nEducation\nSearch\nSearch:\nSearch\nAccount\nMy Account\nRegister\nLog In\nLog Out\nCart\nCheckout\nView Shopping Cart\nShop By\nBody Part\nInjury\nSport\nBrand\nTherapy\nShop By Body Part\nKnee\nShop All Knee\nBraces & Support\nKnee Sleeves\nHinged Braces\nPatella Stabilizers\nBands & Straps\nCustom Knee Braces\nKnee Immobilizers\nKnee Pads\nAccessories\nHot & Cold Therapy\nGet the right brace! Use our Brace Advisor\nAnkle\nShop All Ankle\nWalking Braces\nBraces & Support\nHot & Cold Therapy\nGet the right brace! Use our Brace Advisor\nBack\nShop All Back\nLower Back Braces\nUpper Back / Posture Support\nAbdominal / Rib Support\nHot & Cold Therapy\nGet the right brace! Use our Brace Advisor\nWrist & Thumb\nShop All Wrist & Thumb\nImmobilizers\nBraces & Support\nHot & Cold Therapy\nGet the right brace! Use our Brace Advisor\nShoulder\nShop All Shoulder\nBraces & Support\nImmobilizers / Post-Op\nClavicle Support\nHot & Cold Therapy\nGet the right brace! Use our Brace Advisor\nElbow\nShop All Elbow\nBraces & Support\nProtective Padding\nHot & Cold Therapy\nGet the right brace! Use our Brace Advisor\nNeck\nShop All Neck\nBraces & Support\nHot & Cold Therapy\nGet the right brace! Use our Brace Advisor\nFoot\nShop All Foot\nWalking Boots\nBraces & Support\nShoes\nSocks\nAccessories\nHot & Cold Therapy\nGet the right brace! Use our Brace Advisor\nLeg\nShop All Leg\nBraces & Support\nPost-Op\nHot & Cold Therapy\nGet the right brace! Use our Brace Advisor\nShop All Body Parts >\nShop By Injury\nKnee Injuries\nACL Injury\nChondromalacia\nDislocated Kneecap\nIliotibial Band Syndrome\nMCL/LCL Injury\nMeniscus Tear\nOsteoarthritis of the Knee\nPatellofemoral Pain (Runner's Knee)\nPCL Injury\nPatellar Tendonitis / Jumpers Knee\nHyperextended Knee\nKnee Sprain\nOsgood-Schlatters\nBursitis\nShop All Knee Injuries\nGet the right brace! Use our Brace Advisor\nAnkle Injuries\nAchilles Tendonitis\nAnkle Sprain & Instability\nOsteoarthritis of the Ankle\nShop All Ankle Injuries\nGet the right brace! Use our Brace Advisor\nShoulder Injuries\nRotator Cuff Tear\nShoulder Arthritis\nShoulder Dislocation\nShoulder Instability\nShoulder Impingement\nShoulder/AC Joint Separation\nShop All Shoulder Injuries\nGet the right brace! Use our Brace Advisor\nLeg Injuries\nHamstring Injury\nCalf Strain\nShin Splints\nQuad/Groin Injury\nShop All Leg Injuries\nGet the right brace! Use our Brace Advisor\nBack Injuries\nAbdominal Strains\nHerniated Disc\nLower Back Pain\nSacroiliac Joint Discomfort\nSciatica\nUpper Back Problems\nShop All Back Injuries\nGet the right brace! Use our Brace Advisor\nElbow Injuries\nElbow Arthritis\nElbow Hyperextension\nElbow Tendonitis\nGolfer's Elbow\nTennis Elbow\nShop All Elbow Injuries\nGet the right brace! Use our Brace Advisor\nWrist & Thumb Injuries\nCarpal Tunnel Syndrome\nThumb Injury\nWrist Injury\nShop All Wrist & Thumb Injuries\nGet the right brace! Use our Brace Advisor\nFoot Injuries\nArch Pain\nBall of Foot Pain\nHeel Pain & Spurs\nPlantar Fasciitis\nHigh-Arched Foot / Cavus Foot\nFlat Foot / PTTD\nFoot Arthritis\nDiabetic Foot\nStress Fractures\nBunions\nDrop Foot\nShop All Foot Injuries\nGet the right brace! Use our Brace Advisor\nShop All Injuries >\nShop By Sport\nRunning\nKnee Braces\nAnkle Braces\nCalf & Shin Supports\nFoot Braces\nShop All Running Products\nGet the right brace! Use our Brace Advisor\nBasketball\nKnee Braces\nAnkle Braces\nWrist Braces\nElbow Braces\nCalf & Shin Supports\nShoulder Supports\nShop All Basketball Products\nGet the right brace! Use our Brace Advisor\nLacrosse\nKnee Braces\nAnkle Braces\nElbow Braces\nShoulder Braces\nShop All Lacrosse Products\nGet the right brace! Use our Brace Advisor\nFootball\nKnee Braces\nAnkle Braces\nElbow Supports\nShoulder Braces\nWrist Supports\nBack Braces\nShop All Football Products\nGet the right brace! Use our Brace Advisor\nSoccer\nKnee Braces\nAnkle Braces\nBack Supports\nHead Protection\nShoulder Braces\nWrist Supports\nShop All Soccer Products\nGet the right brace! Use our Brace Advisor\nTennis\nKnee Braces\nAnkle Braces\nElbow Supports\nShop All Tennis Products\nGet the right brace! Use our Brace Advisor\nVolleyball\nKnee Braces\nAnkle Braces\nCalf/Shin Supports\nElbow Braces\nShop All Volleyball Products\nGet the right brace! Use our Brace Advisor\nBaseball\nKnee Braces\nAnkle Braces\nBack Braces\nShoulder Support\nElbow Braces\nWrist Braces\nShop All Baseball & Softball\nGet the right brace! Use our Brace Advisor\nSnowboarding\nKnee Braces\nAnkle Braces\nWrist Supports\nBack Braces\nShop All Snowboarding Products\nGet the right brace! Use our Brace Advisor\nSkiing\nKnee Braces\nAnkle Braces\nBack Braces\nWrist Braces\nShop All Skiing Products\nGet the right brace! Use our Brace Advisor\nHockey\nKnee Braces\nAnkle Braces\nBack Braces\nElbow Braces\nShoulder Braces\nShop All Hockey Products\nGet the right brace! Use our Brace Advisor\nShop All Sports >\nShop By Brand\nDonJoy\nAircast\nDonJoy Advantage\nColpac\nDonJoy Performance\nProcare\nShop All Brands >\nCompex\nHyperIce\nDr Comfort\nChattanooga\nAircast\nDonJoy\nDonJoy Performance\nDonJoy Advantage\nColPac\nProcare\nCompex\nDr Comfort\nExos\nChattanooga\nEmpi\nHyperice\nView All Brands\nShop By Therapy\nTherapy\nHot & Cold Therapy\nColpac Ice Packs\nFast Freeze Pain Relief\nTENS Units\nMuscle Stimulators\nElectrodes\nRehab Products\nProtective Padding\nShop All Therapy >\nHot & Cold Therapy\nColPac Ice Packs\nFast Freeze Pain Relief\nTENS Units\nMuscle Stimulators\nElectrodes\nRehab Products\nProtective Padding\nFitness Roller\nTopical Pain Relief\nShop All\nBrace Advisor\nSale\nEducation\nAbout Us\nCustomer Service\n25% Off Your Purchase + Free Shipping | Code: BBEASTER | SHOP NOW\nOffer Ends 04/22/19 | DETAILS\n25% Off Sitewide* | SHOP NOW\nTake 25% Off Your Order with Code: BBEASTER. Offer ends 04/22/19. Limit 1 coupon per offer. Exclusions apply. Cannot be combined with other coupons or discounts. Not applicable to previous orders. Free ground shipping on all orders in the 48 contiguous states. In-stock, non-custom-non made-to-order products ship in 3-5 business days.\n*Offer excludes Hyperice, DonJoy Performance Bionic\u2122 Reel-Adjust Wrist, DonJoy Performance Bionic\u2122 Reel-Adjust Back, Aircast Walking Boots (Standard, Elite, Short, SP, FP, XP), Aircast Foam Liners, and Compex Edge 2.0. For a complete exclusion list, contact customer service.\nIf you would like to purchase both products eligible for coupon code and products excluded from the promotion, please make two separate transactions. If you have questions, please contact customer service for assistance.\nHome /\nHead & Neck Braces\nHead and Neck Braces & Supports\nCervical collars or neck braces help support and immobilize the neck after injury, neck strain, or whiplash. Whether you need a cervical collar or headgear to protect your head from concussions during soccer, we have the right head or neck support for you. Our bracing experts recommend the following supports for head and neck pain.\nRead More Read Less\nShop By\nCurrently Shopping by\nProduct Type:\nBraces & Supports Remove This Item\nClear All\nFilter\nCategory\nCervical Collars (1)\nProduct Type\nBraces & Supports (1)\nPost-Op (1)\nTherapy Products (3)\nRehab (1)\nCold Therapy (2)\nIntended Use\nDaily Use (1)\nPost-Op/Rehabilitation (1)\nInjuries\nNeck Strain (1)\nBrand\nProCare (1)\nPart of Head & Neck\nNeck (1)\nMaterial\nLatex Free (1)\nBrace Type\nWraparound (1)\nName Price New Best Sellers Top Rated Reviews Count Set Ascending Direction\n1 Item(s)\nShow 12 24 48 96 All\nProCare\nForm Fit Cervical Collar\n$26.99\nExtra 25% Off\nAdd to Cart\nCervical collar to restrict cervical spine flexion, extension and rotation Read More\nSort By Position Name Price New Best Sellers Top Rated Reviews Count Set Ascending Direction\n1 Item(s)\nShow 12 24 48 96 All\nHead and Neck Supports\nNeck braces and cervical collars are ideal for helping heal head and neck injuries by immobilizing the area after an injury has occurred. This is important in order to help prevent movement that could further injure the area. Neck braces are commonly used after events such as car accidents or extreme sports injuries. Paramedics and other types of first responders often use cervical collars to stabilize the neck after an injury. Once the doctor has examined the injury, he or she may choose to keep the patient in the neck brace. They help during rehabilitation by supporting the weight of the head and keeping pressure off any strained neck muscles. A good neck brace or cervical collar not only protects, but they will be comfortable as well. Check out our wide selection of head and neck braces and cervical collars today!\nNewsletter\nSTAY CONNECTED Sign up for the Better Braces Newsletter\nSIGN UP\nBlog\nPinterest\nIntragram\nFacebook\nTwiiter\nYouTube\nBraces & Support\nAnkle\nKnee\nBack Braces\nHead & Neck\nWrist & Thumb\nShoulder\nFoot\nElbow\nLeg Supports\nWalking Boots\nParts & Accessories\nDonJoy Replacement Parts\nAircast Replacement Parts\nCompex Replacement Parts\nTherapy & More\nHot & Cold Therapy\nMuscle Stimulators\nElectrodes\nApparel\nMotionCare\nBrands\nAircast\nDonJoy\nDonJoy Performance\nDonJoy Advantage\nProcare\nDr Comfort\nExos\nCompex\nChattanooga\nEmpi\nHyperice\nFast Freeze\nSaunders\nLearn More\nBlog\nJoin the Club\nShop By Injury\nShop By Sport\nSitemap\nPrivacy Policy\nCustomer Service\nMy Account\nView Cart\nAbout Us\nCustomer Care\nShipping & Rates\nReturns\nContact Us\nCopyright \u00a9 2019 BetterBraces.com\nIndividual results may vary.\nNeither DJO Global, Inc. nor any of its subsidiaries dispense medical advice. The contents of this website do not constitute medical, legal, or any other type of professional advice. Information related to various health, medical, and fitness conditions and their treatment is not meant to be a substitute for the advice provided by a physician or other medical professional. You should not use the information contained herein for diagnosing a health or fitness problem or disease. Rather, please consult your healthcare professional for information on the courses of treatment, if any, which may be appropriate for you.\npowered by Olark live chat software\nPlease wait...\nContinue Shopping Go to Cart\nContinue Shopping Go to Cart\nCall 800-553-6019\nNeed Help?\n\u00d7"""
    keywords = "brands javascript enabled disabled intragram shop facebook browser therapy twiiter"
    query = "can you use a neck brace for a foot injury when do neck braces stop working what neck braces for athletes for pain how to use cervical collar why use neck braces what kind of braces for knee do cervical braces work? which braces are used to protect the cervical spine which neck braces help does a cervical neck brace help a muscle strain"

    doc2: dict = {"all": all, "text": text, "keywords": keywords, "query": query}

    topics = {1: topic}
    docs = {1: [doc1, doc2]}

    return topics, docs


def init_model(model_name, device_id: str = "0"):
    if "qa_evaluator" == model_name:
        model_name = "iarfmoose/bert-base-cased-qa-evaluator"
    elif "monobert" == model_name:
        model_name = "castorini/monobert-large-msmarco-finetune-only"
    elif "bert_base" == model_name:
        model_name = "Capreolus/bert-base-msmarco"
    elif "electra" == model_name:
        model_name = "Capreolus/electra-base-msmarco"
    elif "cross_encoder" == model_name:
        model_name = "cross-encoder/ms-marco-MiniLM-L-6-v2"

    # Look for gpu to use. Will use `cpu` by default if no gpu found.
    # device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    device = torch.device("cuda:" + device_id)

    tokenizer = AutoTokenizer.from_pretrained(model_name)
    model = AutoModelForSequenceClassification.from_pretrained(model_name)
    model.eval()
    model.to(device)
    print("[%s] model %s loaded to %s" % (datetime.datetime.now(), model.name_or_path, device))
    return model, tokenizer, device


def main(model_name, results: str = "./results/trec_bm25", collection_folder: str = "./annotation",
         output_folder: str = "./results/rr", device_id: str = "0", training: bool = False):
    if training:
        results += "_training"
        collection_folder += "_training"
        output_folder += "_training"

    print("[%s] loading model" % datetime.datetime.now())
    model, tokenizer, device = init_model(model_name, device_id)

    print("[%s] loading collection" % datetime.datetime.now())
    # topics, docs = simple_test()
    topics, docs = load_results(results, collection_folder)

    print("[%s] starting to process topics" % datetime.datetime.now())
    model_labels: dict = {}  # topic_id -> [label_1, label_2, ..., label_n]
    for topic_id in topics.keys():
        match_docs = docs[topic_id]
        match_topics = [topics[topic_id]] * len(match_docs)

        model_labels[topic_id] = classify(model, tokenizer, device, match_topics, match_docs)
        print("[%s] %s: ratio=%.4f" % (datetime.datetime.now(), topic_id, sum(model_labels[topic_id]) / len(model_labels[topic_id])))

    save_results(model_labels, results, output_folder, model_name)


if __name__ == "__main__":
    model_name: str = sys.argv[1]
    results: str = "./results/trec_bm25"
    collection_folder: str = "./annotation"
    output_folder: str = "./results/rr"
    device_id: str = "2"
    training: bool = False

    valid_names: list = ["qa_evaluator", "monobert", "bert_base", "electra", "cross_encoder"]

    if len(sys.argv) == 1 or sys.argv[1] == "help" or sys.argv[1] not in valid_names:
        print(
            "python3 pointwise_rr.py qa_evaluator|monobert|bert_base|electra|cross_encoder [<result_file>] [<collection_folder>] [<output_folder>] [0|1|2|3] [training]")
        exit()
    if len(sys.argv) > 2:
        results = sys.argv[2]
    if len(sys.argv) > 3:
        collection_folder = sys.argv[3]
    if len(sys.argv) > 4:
        output_folder = sys.argv[4]
    if len(sys.argv) > 5:
        device_id = sys.argv[5]
    if len(sys.argv) > 6 and sys.argv[6] == "training":
        training = True

    main(model_name, results, collection_folder, output_folder, device_id, training)
