#read in the jsonl files and make the rank
import os
import jsonlines
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--model_name', type=str, required=True)
args = parser.parse_args()

foldername = './query-description'

path_data = os.path.join(foldername, args.model_name)
filenames = os.listdir(path_data)

for fn in filenames:
  qid = fn.rsplit('.', 1)[0]

  output = open("./supportiveness-decimal-ranking/document-score-of-each-topic/" + args.model_name + "/" + qid + "-ranking", 'w')   
 
  with jsonlines.open(os.path.join(path_data,fn)) as f:
    for line in f:
      docno = line["docno"]
      label_indexes = line['label_index']
      #0,1/0
      if label_indexes.count(0) and (not label_indexes.count(2)):
        indices_0 = [index for index, element in enumerate(label_indexes) if element == 0]
        label_confidence_0 = [line['label_confidence'][i] for i in indices_0]
        label_confidence_0_average = max(label_confidence_0) #highest score in the list
        score = round(label_confidence_0_average,4)
        label_class = "0"
      #2,1/2
      elif (not label_indexes.count(0)) and label_indexes.count(2):
        indices_2 = [index for index, element in enumerate(label_indexes) if element == 2]
        label_confidence_2 = [line['label_confidence'][i] for i in indices_2]
        label_confidence_2_average = max(label_confidence_2) #highest score in the list
        score = round(label_confidence_2_average,4)
        label_class = "2"
      #0,1,2
      elif label_indexes.count(0) and label_indexes.count(2): #drop, or maybe use majority vote, we will leave it there for the moment
        score = -2
        label_class = "-2"
      #1
      else:
        indices_1 = [index for index, element in enumerate(label_indexes) if element == 1]
        label_confidence_1 = [line['label_confidence'][i] for i in indices_1]
        label_confidence_1_average = 1 - sum(label_confidence_1) / len(label_confidence_1) #average score
        score = round(label_confidence_1_average,4)
        label_class = "1"

      strings = [qid, "Q0", docno, str(score), label_class]
      output.write(' '.join(strings))
      output.write('\n')
output.close()

#nohup python3 step2-supportivenes-document-score.py --model_name scibert &
#nohup python3 step2-supportivenes-document-score.py --model_name biomed_roberta_base &
#nohup python3 step2-supportivenes-document-score.py --model_name roberta_large &


