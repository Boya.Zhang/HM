# Download Pre-Trained Models

Download pre-trained models from the paper "Fact or Fiction: Verifying Scientific Claims." 

## Source

The script `download-model.sh` is sourced from SciFact project on GitHub. More details can be found at: [SciFact Repository](https://github.com/allenai/scifact).

### Syntax

```
./script/download-model.sh [model-component] [bert-variant] [training-dataset]
```

- `[model-component]`: Use `label`.
- `[bert-variant]`: Choose from the following options:
  - `roberta_large`
  - `scibert`
  - `biomed_roberta_base`
- `[training-dataset]`: Use `fever_scifact`.