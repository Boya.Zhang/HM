from xml.dom import minidom
import os
import json
import jsonlines
import argparse

import torch
from transformers import AutoConfig, AutoTokenizer, AutoModelForSequenceClassification
from tqdm import tqdm

parser = argparse.ArgumentParser()
parser.add_argument('--model', type=str, required=True)
parser.add_argument('--model_name', type=str, required=True)
args = parser.parse_args()

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
print(f'Using device "{device}"')

tokenizer = AutoTokenizer.from_pretrained(args.model)
config = AutoConfig.from_pretrained(args.model, num_labels=3)
model = AutoModelForSequenceClassification.from_pretrained(args.model, config=config).eval().to(device)

dom = minidom.parse("/Data/topics/misinfo-2021-topics.xml")
numbers = dom.getElementsByTagName("number")
queries_1 = dom.getElementsByTagName("query")
queries_2 = dom.getElementsByTagName("description")

def format_input_window(text, window_size=5, cutoff=5):
    sentences = []
    for sentence in text.split("\n"):
        if len(sentence.strip()) > 0 and (cutoff <= 0 or len(sentence.split()) > cutoff):
            sentences.append(sentence.strip())
    pairs = []
    for start_idx in range(0, len(sentences), window_size):
        end_idx = min(start_idx + window_size, len(sentences))
        pairs.append([" ".join(sentences[start_idx:end_idx])])
    return pairs

def encode(sentences, claims):
    text = list(zip(sentences, claims))
    encoded_dict = tokenizer.batch_encode_plus(
        text,
        pad_to_max_length=True,
        return_tensors='pt'
    )
    if encoded_dict['input_ids'].size(1) > 512:
        encoded_dict = tokenizer.batch_encode_plus(
            text,
            max_length=512,
            pad_to_max_length=True,
            truncation_strategy='only_first',
            return_tensors='pt'
        )
    encoded_dict = {key: tensor.to(device)
                  for key, tensor in encoded_dict.items()}
    return encoded_dict

with torch.no_grad():
    for i in range(len(numbers)):
        # query+description
        claim_1 = queries_1[i].firstChild.data
        claim_2 = queries_2[i].firstChild.data
        claim = claim_1 + ". " + claim_2
        path_data = numbers[i].firstChild.data
        filenames = os.listdir(path_data)
        output = jsonlines.open("./query-description/"+args.model_name+"/"+path_data+".jsonl", 'w')

        for fn in filenames:
            with open(os.path.join(path_data,fn)) as f:
                data = json.load(f)
                large_paragraph = data["text"]
                useful_sentences = format_input_window(large_paragraph, window_size=5, cutoff=5)
                round_label_scores_list = []
                label_index_list = []
                label_confidence_list = []
                for evidence in useful_sentences:
                    encoded_dict = encode([str(evidence)], [claim])
                    label_scores = torch.softmax(model(**encoded_dict)[0], dim=1)[0]
                    round_label_scores = [round(num, 4) for num in label_scores.tolist()]
                    label_index = label_scores.argmax().item()
                    label_confidence = round(label_scores[label_index].item(), 4)

                    round_label_scores_list.append(round_label_scores)
                    label_index_list.append(label_index)
                    label_confidence_list.append(label_confidence)

                output.write({
                    'docno': data["docno"],
                    'label_index': label_index_list,
                    'label_confidence': label_confidence_list,
                    'label_score': round_label_scores_list
                })
output.close()

#LABELS = ['CONTRADICT', 'NOT_ENOUGH_INFO', 'SUPPORT'] as [0,1,2]
#In the ranking, we want [1,0,-1] discrete or contiunous
#the strategy for continuous ranking is the same as in [1,0,-1]

# nohup python3 step1-supportiveness-sentence-score.py --model ./supportiveness_model/label_scibert_fever_scifact --model_name scibert &
# nohup python3 step1-supportiveness-sentence-score.py --model ./supportiveness_model/label_biomed_roberta_base_fever_scifact --model_name biomed_roberta_base &
# nohup python3 step1-supportiveness-sentence-score.py --model ./supportiveness_model/label_roberta_large_fever_scifact --model_name roberta_large &