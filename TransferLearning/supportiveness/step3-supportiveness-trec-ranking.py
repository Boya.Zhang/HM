import os
import pandas as pd
import argparse

def modify_scores(label_class_list, score_list):
    if label_class_list.count(0) and label_class_list.count(2):
        if label_class_list.count(0) > label_class_list.count(2):
            indices = [i for i, x in enumerate(label_class_list) if x == 2]
        else:
            indices = [i for i, x in enumerate(label_class_list) if x == 0]

        for i in indices:
            score_list[i] = -score_list[i]
    return score_list

def process_file(file_path, output_folder, tag):
    rank_csv = pd.read_csv(file_path, delimiter=" ")
    rank_csv['score'] = modify_scores(rank_csv['label_class'].tolist(), rank_csv['score'].tolist())
    sorted_rank_csv = rank_csv.sort_values(by=["score"], ascending=False, ignore_index=True)
    sorted_rank_csv["rank"] = range(1, len(sorted_rank_csv) + 1)
    sorted_rank_csv["tag"] = tag

    output_file_path = os.path.join(output_folder, os.path.basename(file_path))
    sorted_rank_csv[['qid', 'Q0', 'docno', 'rank', 'score', 'tag']].to_csv(output_file_path, sep=' ', index=False, header=False)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--path_data', type=str, required=True, help='Path to the data folder')
    parser.add_argument('--output_folder', type=str, required=True, help='Path to the output folder')
    parser.add_argument('--tag', type=str, required=True, help='Tag to append to each row')
    args = parser.parse_args()

    if not os.path.exists(args.output_folder):
        os.makedirs(args.output_folder)

    for filename in os.listdir(args.path_data):
        file_path = os.path.join(args.path_data, filename)
        process_file(file_path, args.output_folder, args.tag)

if __name__ == "__main__":
    main()

#output_folder = './supportiveness-decimal-ranking/trec-ranking-of-each-topic'
#combine all the final result files to one
# cat ./supportiveness-decimal-ranking/trec-ranking-of-each-topic/scibert/* > supportiveness-rank-decimal-scibert