
# Supportiveness Ranking Generation in Transfer Learning

## Overview

The code is divided into three main steps:

1. **`step1-supportiveness-sentence-score.py`**
   - **Purpose**: Reads from the query, description, and text of retrieved documents. Assigns a score to each sentence in the documents, comparing it to the combined query and description.
   - **Input**: Query and description files, retrieved documents.
   - **Output**: Sentence-level scores.

2. **`step2-supportiveness-document-score.py`**
   - **Purpose**: Generates document ranking scores based on the sentence scores for each topic.
   - **Input**: Sentence-level scores.
   - **Output**: Document-level scores.

3. **`step3-supportiveness-trec-ranking.py`**
   - **Purpose**: Generates the final TREC ranking file based on the document scores.
   - **Input**: Document-level scores.
   - **Output**: TREC ranking file.

### Step 1: Sentence Scoring

```bash
python step1-supportiveness-sentence-score.py --arg1 [value] --arg2 [value]
```

### Step 2: Document Scoring

```bash
python step2-supportiveness-document-score.py --arg1 [value] --arg2 [value]
```

### Step 3: Generating TREC Rankings

```bash
python step3-supportiveness-trec-ranking.py --arg1 [value] --arg2 [value]
```