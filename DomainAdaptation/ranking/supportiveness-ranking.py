import pandas as pd

# Function to read CSV with error handling
def read_csv(filepath):
    try:
        return pd.read_csv(filepath)
    except FileNotFoundError:
        print(f"File not found: {filepath}")
        return pd.DataFrame()

# Read in the CSV files
df1 = read_csv('../prediction/data/document_bm25.csv')
df2 = read_csv('../prediction/results/supportiveness_biobert_test_results.csv')
df3 = read_csv('../prediction/results/credibility_bigbird_test_results.csv')

# Rename columns in df3
df3.rename(columns={"Prediction": "Prediction-Credible", 
                    "Confidence_Score": "Confidence_Score-Credible"}, inplace=True)

# Merge the dataframes side by side
merged_df = pd.concat([df1, df2, df3], axis=1)

# Filter and rank operations
credible_df = merged_df[merged_df['Prediction-Credible'] == 1]
top_credible_df = credible_df.groupby('qid').apply(lambda x: x.nlargest(10, 'Confidence_Score-Credible')).reset_index(drop=True)
topics_with_zero = top_credible_df[top_credible_df['Prediction'] == 0]['qid'].unique()

# Create and merge 'stance_df'
stance_df = pd.DataFrame({'stance': 1}, index=merged_df['qid'].unique())
stance_df.loc[topics_with_zero, 'stance'] = 0
merged_df = merged_df.merge(stance_df, left_on='qid', right_index=True)

# Add 'score' column and rank
merged_df['score'] = merged_df['Confidence_Score'].where(merged_df['Prediction'] == merged_df['stance'], -merged_df['Confidence_Score'])
merged_df['rank'] = merged_df.groupby('qid')['score'].rank(ascending=False, method='min').astype(int)
merged_df.sort_values(['qid', 'rank'], inplace=True)
merged_df.reset_index(drop=True, inplace=True)

# Prepare final DataFrame for export
merged_df['Q0'] = 'Q0'
merged_df['label'] = 'stance_biobert'
selected_columns = ['qid', 'Q0', 'docno', 'rank', 'score', 'label']
selected_df = merged_df[selected_columns]

# Save the dataframe as TSV file
selected_df.to_csv('./ranking-lists/stance_biobert', sep=' ', index=False, header=False)