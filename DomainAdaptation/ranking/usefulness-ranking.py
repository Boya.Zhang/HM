import pandas as pd

def read_csv(filepath):
    try:
        return pd.read_csv(filepath)
    except FileNotFoundError:
        print(f"File not found: {filepath}")
        return pd.DataFrame()

# Read the CSV files
df1 = read_csv('../prediction/data/document_bm25.csv')
df2 = read_csv('../prediction/results/usefulness_biobert_test_results.csv')

# Check if the dataframes have the same number of rows
if len(df1) != len(df2):
    print("Error: The dataframes have different numbers of rows.")
else:
    # Merge the two dataframes side by side
    merged_df = pd.concat([df1, df2], axis=1)

    # Modify the confidence score based on the prediction
    merged_df.loc[merged_df['Prediction'] == 0, 'Confidence_Score'] *= -1

    # Rank the docno for each qid based on confidence score
    merged_df['rank'] = merged_df.groupby('qid')['Confidence_Score'].rank(ascending=False, method='dense').astype(int)

    # Sort the dataframe by qid and rank
    merged_df.sort_values(['qid', 'rank'], inplace=True)

    # Reindex the dataframe
    merged_df.reset_index(drop=True, inplace=True)

    # Create Q0 and label columns
    merged_df['Q0'] = 'Q0'
    merged_df['label'] = 'bigbird_credible'

    # Select columns for the TSV file
    selected_columns = ['qid', 'Q0', 'docno', 'rank', 'Confidence_Score', 'label']
    selected_df = merged_df[selected_columns]

    # Rename columns for the TSV file
    selected_df.columns = ['qid', 'Q0', 'docno', 'rank', 'score', 'label']

    # Save the dataframe as TSV file
    selected_df.to_csv('./ranking-lists/useful_biobert', sep=' ', index=False, header=False)