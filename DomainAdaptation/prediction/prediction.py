import pandas as pd
import torch
from torch.utils.data import Dataset, DataLoader
from transformers import AutoTokenizer, AutoModelForSequenceClassification
import torch.nn.functional as F
import os
import argparse
import numpy as np

def load_data():
    return pd.concat([
        pd.read_csv('./data/document_bm25.csv'), 
        pd.read_csv('./data/document_shortened_bm25.csv')
    ], axis=1)

class TrecDataset(Dataset): 
    def __init__(self, data, tokenizer, use_full_text):
        self.data = data
        self.tokenizer = tokenizer
        self.use_full_text = use_full_text

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        item = self.data.iloc[idx]
        text_pair = item['processed_paragraph'] if not self.use_full_text else item['text']
        encoded_item = self.tokenizer.encode_plus(
            text=item['claim'],
            text_pair=text_pair,
            add_special_tokens=True,
            max_length=512,
            padding='max_length',
            return_tensors='pt',
            truncation=True
        )
        return {
            'input_ids': encoded_item['input_ids'].flatten(),
            'attention_mask': encoded_item['attention_mask'].flatten()
        }

def main(args):
    merged_df = load_data()

    model_details = {
        'usefulness': ('dmis-lab/biobert-base-cased-v1.1', './model/model-biobert-base-usefulness-short', './results/usefulness_biobert_test_results.csv', False, 128),
        'supportiveness': ('dmis-lab/biobert-base-cased-v1.1', './model/model-biobert-supportiveness-short', './results/supportiveness_biobert_test_results.csv', False, 128),
        'credibility': ('google/bigbird-roberta-base', './model/model-bigbird-credibility', './results/credibility_bigbird_test_results.csv', True, 16)
    }

    if args.criteria_type not in model_details:
        raise ValueError("Invalid criteria type. Must be one of: 'usefulness', 'supportiveness', 'credibility'")

    model_name, save_dir, output_dir, use_full_text, batch_size = model_details[args.criteria_type]

    tokenizer = AutoTokenizer.from_pretrained(model_name)
    test_dataset = TrecDataset(merged_df, tokenizer, use_full_text)
    test_dataloader = DataLoader(test_dataset, batch_size=batch_size) # the batch size depends on the model

    # Load pre-trained model
    model = AutoModelForSequenceClassification.from_pretrained(model_name, num_labels=2)

    # Load saved model weights
    model.load_state_dict(torch.load(os.path.join(save_dir, 'best_model.pt')))
    model.eval()

    # Setting up device (GPU/CPU)
    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
    model = model.to(device)

    # Prediction Loop
    all_predictions = []
    all_probabilities = []

    # Model evaluation
    for batch in test_dataloader:
        input_ids = batch['input_ids'].to(device)
        attention_mask = batch['attention_mask'].to(device)

        with torch.no_grad():
            outputs = model(input_ids, attention_mask=attention_mask)

        logits = outputs.logits # Get the logits
        probabilities = F.softmax(logits, dim=-1) # Get the probabilities
        confidence_scores = torch.max(probabilities, dim=-1).values # Get the confidence scores
        confidence_scores = confidence_scores.cpu().numpy() # Convert to numpy array

        if args.criteria_type == 'supportiveness':
            threshold = 0.995 # Set the best threshold
            predicted_labels = np.where(confidence_scores > threshold, predicted_labels, 0) # Adjust predictions based on the threshold

        else: # For usefulness and credibility
            predicted_labels = torch.argmax(logits, dim=-1).cpu().numpy() # Get the predicted labels

        all_predictions.extend(predicted_labels)
        all_probabilities.extend(confidence_scores)

    # Save Results to CSV
    result = pd.DataFrame({'Prediction': all_predictions, 'Confidence_Score': all_probabilities})
    result.to_csv(output_dir, index=False)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Generate the predictions with BERT-style models on TREC HM datasets.') # Create the parser
    parser.add_argument('--criteria_type', type=str, required=True, help='The criteria type to make the prediction. Must be one of: usefulness, supportiveness, credibility') # Add the arguments
    args = parser.parse_args() # Parse the arguments
    main(args) # Run the main function